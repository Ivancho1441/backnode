var express = require('express');
var bodyparser = require('body-parser');
var jsonQuery = require('json-query');
var requestJson = require('request-json');
var session = require('express-session');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

var app = express();
app.set('trust proxy', 1);
app.use(bodyparser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:8081");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(session({
    secret: 'ivbank',
    resave: false,
    saveUninitialized: true
}));

const dbUsser = 'node';
const dbPass = '123qwe123qwe';
const dbName = 'backtechuivan';
const urlMongo = 'mongodb://'+dbUsser+':'+dbPass+'@ds135619.mlab.com:35619/'+dbName;

const sessionValidity = 1000 * 60 * 10;

const findDocuments = function(db, collection, query, callback) {
  console.log('Query a ejecutar : ', query);
  collection.find(query).toArray(function(err, result) {
    assert.equal(err, null);
    callback(result);
  });
}

const insertDocuments = function(db, collection, query, callback) {
  console.log('Query a insertar : ', query);
  collection.insert(query, function(err, result) {
    assert.equal(err, null);
    callback(result);
  });
}

var auth = function(req, res, next) {
    console.log('token en sesion: '+req.session.token);
    console.log('token en body: '+req.body.token);
    if (req.session.token == req.body.token) {
        return next();
    } else {
        return res.send('{ \"error\":\"No autorizado\" }');
    }
};

app.post('/v0/login', function (req, res) {
    MongoClient.connect(urlMongo, function(err, client) {
    assert.equal(null, err);
    const db = client.db(dbName);
    const collection = db.collection('users');
    findDocuments(db, collection, req.body, function(result) {
      client.close();
      console.log('Esto es lo recibido en el post de login: ', result);
      if (result.length == 1) {
        req.session.mail = result[0].mail;
        req.session.profile = result[0].profile;
        req.session.idCliente = result[0]._id;
        req.session.token = Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
        req.session.cookie.expires = new Date(Date.now() + sessionValidity)
        res.send('{ \"token\":\"'+req.session.token+'\" }');
      } else {
        res.send('{ \"error\":\"Datos invalidos\" }')
      }
    });
  });
})

app.post('/v0/getTransactions', auth, function(req, res) {
  var queryMongo = {};
  queryMongo.idCliente = req.session.idCliente;
  MongoClient.connect(urlMongo, function(err, client) {
    assert.equal(null, err);
    const db = client.db(dbName);
    const collection = db.collection('transactions');
    findDocuments(db, collection, queryMongo, function(result) {
      client.close();
      var respuesta = {};
      respuesta.listado = result;
      console.log('Esto es lo recibido en el post de movimientos: ', respuesta);
      res.send(respuesta);
    });
  });
})

app.post('/v0/insertTransaction', auth, function(req, res) {
  var queryMongo = req.body;
  queryMongo.idCliente = req.session.idCliente;
  delete queryMongo.token;
  MongoClient.connect(urlMongo, function(err, client) {
    assert.equal(null, err);
    const db = client.db(dbName);
    const collection = db.collection('transactions');
    insertDocuments(db, collection, queryMongo, function(result) {
      client.close();
      console.log('Esto es lo recibido en el post de Insert movimientos: ', result);
      res.send('{ \"status\":\"OK\" }');
    });
  });
})

app.post('/v0/getCustomer', auth, function (req, res) {
  console.log('Entro a getCustomer con: '+req.session.token);
  var queryMongo = {};
  queryMongo.mail = req.session.mail;
  MongoClient.connect(urlMongo, function(err, client) {
    assert.equal(null, err);
    const db = client.db(dbName);
    const collection = db.collection('users');
    findDocuments(db, collection, queryMongo, function(result) {
      client.close();
      var respuesta = {};
      respuesta.name = result[0].name;
      respuesta.lastName = result[0].lastName;
      respuesta.profile = result[0].profile;
      respuesta.token = req.session.token;
      console.log(respuesta);
      res.send(respuesta);
    });
  });
})

app.post('/v0/insertCustomer', auth, function(req, res) {
  var queryMongo = req.body;
  delete queryMongo.token;
  if (req.session.profile =! 'admin') {
    res.send('{ \"error\":\"No autorizado\" }');
  }
  MongoClient.connect(urlMongo, function(err, client) {
    assert.equal(null, err);
    const db = client.db(dbName);
    const collection = db.collection('users');
    insertDocuments(db, collection, queryMongo, function(result) {
      client.close();
      console.log('Esto es lo recibido en el post de Insert users: ', result);
      res.send('{ \"status\":\"OK\" }');
    });
  });
})

app.post('/v0/logout', auth, function (req, res) {
    req.session.destroy();
    return res.send('{ \"status\":\"OK\" }');
});

app.listen(3000);
console.log('Servidor iniciado')
